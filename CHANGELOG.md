# CHANGELOG

## 0.0.1 - Initial commit

- Move code from `graphlang`([crates.io](https://crates.io/crates/graphlang),[Repository](https://gitlab.com/dBnx/graphlang)) project
- Make it into a (rudimentary) crate
