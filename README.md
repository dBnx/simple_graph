# simplegraph - stupid graph implementation

![Crates.io](https://img.shields.io/crates/v/simple_graph)
![Crates.io](https://img.shields.io/crates/l/simple_graph)
![Pipeline](https://gitlab.com/dBnx/simple_graph/badges/main/pipeline.svg)
![docs.rs](https://img.shields.io/docsrs/simple_graph)

**NOTE: This crate is still highly experimental and API changes should be expected!
Performance is currently also not a concern. I highly discourage any use except for
experiments.**

## What and why?

This crate contains the core structures and operations used in the
`graphlang`([crates.io](https://crates.io/crates/graphlang),[Repository](https://gitlab.com/dBnx/graphlang))
project and it's goal is to simplify and modularize the codebase.

## TODO's

- [ ] Library: Eventually use the `graph_builder`-crate
- [ ] Library: Make it more generic s.t. it is actually usable by other projects
- [ ] Library: Implement a better partial isomorphism algorithm - maybe VL2? Should be a major speedup
