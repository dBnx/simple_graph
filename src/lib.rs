#![deny(unsafe_code)]
#![warn(
    clippy::all,
    clippy::cargo,
    clippy::complexity,
    clippy::correctness,
    clippy::nursery,
    clippy::perf,
    clippy::pedantic,
    clippy::style,
    clippy::suspicious,
    clippy::clone_on_ref_ptr,
    rust_2018_idioms,
    rust_2021_compatibility
)]
#![allow(clippy::clone_on_copy)]

mod graph;
mod graphoperations;
mod primitives;
pub use graph::Graph;
pub use graphoperations::{are_graphs_isomorph, match_subgraph};
pub use primitives::{Edge, Index, Isomorphism, Label, Node, Production};
