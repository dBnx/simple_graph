use serde::{Deserialize, Serialize};
use std::{collections::HashMap, fmt::Display, path::Path};

use crate::Graph;

/// Trait alias `Index` without language support for trait aliases
pub trait Index:
    std::fmt::Debug + std::cmp::Eq + std::hash::Hash + Serialize + Clone + Default
{
}
impl<
        T: std::fmt::Debug + std::cmp::Eq + std::hash::Hash + Serialize + Clone + Default + ?Sized,
    > Index for T
{
}

/// Trait alias `Label` without language support for trait aliases
pub trait Label: std::cmp::Eq + std::fmt::Debug + std::hash::Hash + Serialize + Clone {}
impl<T: std::cmp::Eq + std::fmt::Debug + std::hash::Hash + Serialize + Clone + ?Sized> Label for T {}

/// Represents a single `Node`. It has an `id`entifier  and an optional `label`,
/// which can be removed by using the zero-sized type `()`
#[derive(Serialize, Deserialize, PartialEq, Eq, Clone, Debug)]
pub struct Node<I: Index, NL: Label> {
    pub id: I,
    pub label: NL,
}

impl<I: Index, NL: Label> Node<I, NL> {
    /// Create a new `Node` with an `id`, that is used to differentiate/identify the
    /// node which has therefore to be unique and a `label`, which determines the
    /// 'type'.
    pub const fn new(id: I, label: NL) -> Self {
        Self { id, label }
    }

    /// Checks if the node has the same label as the other node `n`.
    pub fn is_isomorph_to(&self, n: &Self) -> bool {
        self.label == n.label
    }
}

/// Represents a single directed `Edge` and an optional `label`,
/// which can be removed by using the zero-sized type `()`.
/// It is recommended to use the corresponding `::new_*` methods.
#[derive(Serialize, Deserialize, PartialEq, Eq, Clone, Debug)]
pub struct Edge<I: Index, EL: Label> {
    pub from: I,
    pub to: I,
    pub label: EL,
}

impl<I: Index, EL: Label> Edge<I, EL> {
    /// Create a labeled `Edge`. Note that the current implementation
    /// does not handle/respect edge labels.
    pub const fn new_label(from: I, to: I, label: EL) -> Self {
        Self { from, to, label }
    }
}

impl<I: Index> Edge<I, ()> {
    /// Create an unlabeled `Edge`.
    pub const fn new_unlabeled(from: I, to: I) -> Self {
        Self {
            from,
            to,
            label: (),
        }
    }
}

// TODO: Make inner private
/// An `Isomorphism` is a map from the matching graph to the matched subgraph. You most likely
/// don't want to construct it, but use the `match_subgraph`-method.
#[derive(Serialize, Deserialize, Default, PartialEq, Eq, Clone, Debug)]
pub struct Isomorphism<I: Index>(pub HashMap<I, I>);

impl<I: Index> Isomorphism<I> {
    /// Translates a single node, if possible or else returns `None`.
    #[must_use]
    pub fn translate_node<NL: Label>(&self, node: &Node<I, NL>) -> Option<Node<I, NL>> {
        Some(Node::new(
            self.0.get(&node.id)?.to_owned(),
            node.label.clone(),
        ))
    }

    /// Checks if the preimage contains all nodes given by `nodes`.
    #[must_use]
    pub fn contains_nodes<NL: Label>(&self, nodes: &[Node<I, NL>]) -> bool {
        nodes.iter().all(|n| self.0.get(&n.id).is_some())
    }

    /// Checks if there exists a mappings for every node id in `nodes`,
    /// where each entry constitutes the lhs of one.
    #[must_use]
    pub fn contains_nodeids_on_lhs(&self, nodes: &[&I]) -> bool {
        nodes.iter().all(|n| self.0.get(n).is_some())
    }

    /// Checks if there exists a mappings for every node in `nodes`,
    /// where each entry constitutes the rhs of one.
    #[must_use]
    pub fn contains_nodeids_on_rhs(&self, nodes: &[&I]) -> bool {
        nodes.iter().all(|n| {
            for v in self.0.values() {
                if &v == n {
                    return true;
                }
            }
            false
        })
    }

    /// Checks if there exists a mapping, where `node` constitutes
    /// the lhs.
    #[must_use]
    pub fn contains_nodeid_on_lhs(&self, node: &I) -> bool {
        self.contains_nodeids_on_lhs(&[node])
    }

    /// Checks if there exists a mapping, where `node` constitutes
    /// the rhs.
    #[must_use]
    pub fn contains_nodeid_on_rhs(&self, node: &I) -> bool {
        self.contains_nodeids_on_rhs(&[node])
    }

    // Check if there is no overlap between self & other. This definition is up to debate
    // (and tests).
    #[must_use]
    pub fn can_be_safely_merged(&self, other: &Self) -> bool {
        for (k, v) in &self.0 {
            if other.contains_nodeids_on_lhs(&[k]) || other.contains_nodeids_on_rhs(&[v]) {
                return false;
            }
        }
        true
    }
}

impl<I: Index, T: AsRef<[(I, I)]>> From<T> for Isomorphism<I> {
    fn from(list: T) -> Self {
        let mut iso = Self::default();
        for (from, to) in list.as_ref() {
            iso.0.insert(from.clone(), to.clone());
        }
        iso
    }
}
/*
impl<'a, I: Index + 'a> From<&'a [(I,I)]> for Isomorphism<I>
{
    fn from(list: &[(I,I)]) -> Self {
        let mut iso = Isomorphism::default();
        for (from, to) in list {
            iso.0.insert(from.clone(), to.clone());
        }
        iso
    }
}
*/

/// A double-pushout production (DPO). For a description of the theory behind
/// it please refer to the crates `README.md`.
///
/// It is a rule how to transform a specific part of an `Graph` into another and
/// the core of a `GraphGrammar`.
#[derive(Serialize, Deserialize, PartialEq, Eq, Clone, Debug)]
pub struct Production<I: Index, NL: Label, EL: Label> {
    lhs: Graph<I, NL, EL>,
    #[serde(alias = "interface")]
    int: Graph<I, NL, EL>,
    rhs: Graph<I, NL, EL>,
}

impl<I: Index, NL: Label, EL: Label> Production<I, NL, EL> {
    #[must_use]
    pub const fn new(
        lhs: Graph<I, NL, EL>,
        interface: Graph<I, NL, EL>,
        rhs: Graph<I, NL, EL>,
    ) -> Self {
        Self {
            lhs,
            int: interface,
            rhs,
        }
    }

    #[must_use]
    pub fn statisfies_gluing_condition(&self) -> bool {
        let _ = &self;
        unimplemented!()
    }

    // TODO: Classification methods
}

impl<I: Index> From<Production<I, &str, ()>> for Production<I, String, ()> {
    fn from(g: Production<I, &str, ()>) -> Self {
        Self {
            lhs: g.lhs.into(),
            int: g.int.into(),
            rhs: g.rhs.into(),
        }
    }
}

// TODO: Generalize s.t. Index can be used instead of u32
fn find_additional_ids<NL: Label, EL: Label>(graph: &Graph<u32, NL, EL>, count: u32) -> Vec<u32> {
    let max = 1 + graph.nodes.iter().map(|n| n.id).max().unwrap();
    (max..(max + count)).collect()
}

/// Finds all ids in `graph_to_translate` that are not covered by `isomorphism` and creates new mappings
/// using `ids`.  
fn fill_unmapped_indices<NL: Label, EL: Label>(
    //graph_to_translate: &Graph<u32, NL, EL>,
    production: &Production<u32, NL, EL>,
    isomorphism: &mut Isomorphism<u32>,
    ids: &[u32],
) {
    // For all nodes in RHS \ LHS create a mapping using an id from `ids`
    let new_mappings = production
        .rhs
        .nodes
        .iter()
        .map(|n| n.id)
        .filter(|id| !production.lhs.nodes.iter().any(|n| &n.id == id))
        .zip(ids)
        .map(|(l, r)| (l.clone(), r.clone()))
        .collect::<Vec<_>>();

    isomorphism.0.extend(new_mappings);
}

/// Adds mappings to an isomorphism for new nodes, that get created by applying the production.
/// Currently only works for u32 specialization. If this is generalized the whole lib can be
/// generic w.r.t. indices.
fn extend_isomorphism_for_unmapped_ids<NL: Label, EL: Label>(
    production: &Production<u32, NL, EL>,
    graph: &Graph<u32, NL, EL>,
    iso: &mut Isomorphism<u32>,
) {
    let n_new_ids_needed = production.rhs.nodes.len() - production.lhs.nodes.len();
    #[allow(clippy::cast_possible_truncation)]
    let additional_ids = find_additional_ids(graph, n_new_ids_needed as u32);
    fill_unmapped_indices(production, iso, &additional_ids);
}

impl<NL: Label, EL: Label> Production<u32, NL, EL> {
    // TODO: Should be Result
    pub fn apply_inplace(&self, g: &mut Graph<u32, NL, EL>) -> Option<()> {
        let mut isomorphism = crate::graphoperations::match_subgraph(g, &self.lhs)?;
        log::warn!("      Apply to graph : {:?}", &g);
        log::warn!("      Find match for : {:?}", &self.lhs);
        log::warn!("Found p. isomorphism : {:?}", &isomorphism);
        log::warn!("     Removing from g : {:?}", &(&self.lhs - &self.int));
        log::warn!("    Inserting into g : {:?}", &(&self.rhs - &self.int));

        extend_isomorphism_for_unmapped_ids(self, g, &mut isomorphism);
        log::warn!("    Extended p. iso. : {:?}", &isomorphism);

        g.remove(&(&self.lhs - &self.int).translate_copy(&isomorphism));
        g.insert(&(&self.rhs - &self.int).translate_copy(&isomorphism));
        // Add all edges that are new from int to rhs
        g.edges.extend(
            self.rhs
                .edges
                .iter()
                .filter(|e| !self.int.edges.contains(e))
                .filter(|e| !(&self.rhs - &self.int).edges.contains(e))
                .map(|e| {
                    Edge::new_label(
                        isomorphism.0[&e.from],
                        isomorphism.0[&e.to],
                        e.label.clone(),
                    )
                }),
        );

        g.cleanup_edges();
        Some(())
    }

    // TODO: Should be Result
    #[must_use]
    pub fn apply(&self, g: &Graph<u32, NL, EL>) -> Option<Graph<u32, NL, EL>> {
        let mut g = g.clone();
        self.apply_inplace(&mut g)?;
        Some(g)
    }
}

//  ------------------------------ DOT FEATURE ------------------------------ Beg
impl<I: Index, NL: Label + Display, EL: Label> Production<I, NL, EL> {
    // Write graph using the dot-format to `output`. It can be easily visualized using external
    // tools e.g. 'xdot'.
    //
    // # Examples
    //
    // ```
    // use graphlang::Production;
    // # fn create_production_somehow() -> Production<u32,&'static str,()> {
    // #   graphlang::predefined::string_grammar().productions.iter().next().unwrap().1.clone()
    // # }
    // # fn test() -> std::io::Result<()> {
    // let prod: Production<_,_,_> = create_production_somehow();
    // let mut f = std::fs::File::create("Example.gv")?;
    // prod.write_dot_to(&mut f)?;
    // #   Ok(())
    // # }
    // # test().expect("Doc test for write_dot_to works");
    // ```
    //
    // # Errors
    //
    // Fails if a label or the name of a graph does not conform to the dot file
    // identifier grammar. Spaces are automatically replaced by underscores, but
    // otherwise only latin letters and numbers are allowed. The first character
    // must be a letter.
    //
    // This generates an invalid dot file, as it contains multiple
    //pub fn write_dot_to<W: std::io::Write>(&self, output: &mut W) -> std::io::Result<()> {
    //    dot::render(&self.lhs, output)?;
    //    dot::render(&self.int, output)?;
    //    dot::render(&self.rhs, output)?;
    //    Ok(())
    //}

    /// Write graph using the dot-format to a file named `file_name`. Note that the file
    /// ending `gv` is automatically added if not already present. If the file should be saved
    /// with another file ending consider using `write_dot_to`, as it is a simple
    /// wrappar around it. Another used file ending is `.dot`.
    ///
    /// # Examples
    ///
    /// ```
    /// use std::path::Path;
    /// use graphlang::Production;
    /// # fn create_production_somehow() -> Production<u32,&'static str,()> {
    /// #   graphlang::predefined::string_grammar().productions.iter().next().unwrap().1.clone()
    /// # }
    /// # fn test() -> std::io::Result<()> {
    /// let prod: Production<_,_,_> = create_production_somehow();
    /// prod.write_dot_to_file(Path::new("someproduction"))?;
    /// // Will be saved as `someproduction.gv`
    /// #   Ok(())
    /// # }
    /// # test().expect("Doc test for write_dot_to works");
    /// ```
    ///
    /// # Errors
    ///
    /// Fails if a label or the name of a graph does not conform to the dot file
    /// identifier grammar. Spaces are automatically replaced by underscores, but
    /// otherwise only latin letters and numbers are allowed. The first character
    /// must be a letter.
    ///
    /// # Panics
    ///
    /// Panics if `file_name` does not contain a valid file name.
    /// ...
    pub fn write_dot_to_file(&self, file_name: &Path) -> std::io::Result<()> {
        let write_with_postfix = |g: &Graph<_, _, _>, postfix| -> std::io::Result<()> {
            let mut base_name = file_name.file_stem().unwrap().to_os_string();
            base_name.push(postfix);

            let mut file_name = file_name.with_file_name(base_name);
            file_name = file_name.with_extension("gv");

            g.write_dot_to_file(&file_name)
        };
        write_with_postfix(&self.lhs, "_lhs")?;
        write_with_postfix(&self.int, "_int")?;
        write_with_postfix(&self.rhs, "_rhs")
    }
}
//  ------------------------------ DOT FEATURE ------------------------------ End

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn deserialize_reference_graph() {
        type N = Node<usize, &'static str>;
        type E = Edge<usize, ()>;
        let g = Graph {
            name: None,
            nodes: vec![N { id: 0, label: "a" }, N { id: 1, label: "a" }],
            edges: vec![E {
                from: 0,
                to: 1,
                label: (),
            }],
        };
        let s = std::fs::read_to_string("./tests/minimal.graph.json").unwrap();
        let d = serde_json::from_str::<Graph<_, _, _>>(&s).unwrap();
        assert_eq!(&g, &d);
    }

    #[test]
    fn serialize_and_deserialize_graph() {
        let g = Graph {
            name: None,
            nodes: vec![Node::new(0, "a"), Node::new(1, "a")],
            edges: vec![Edge::new_unlabeled(0, 1)],
        };
        let s = serde_json::to_string_pretty(&g).unwrap();
        let d = serde_json::from_str::<Graph<_, _, _>>(&s).unwrap();
        assert_eq!(&g, &d);
    }
}
